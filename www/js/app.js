/**
 * Created by ufogxl on 16/6/17.
 */
angular.module('App',['ionic','Lists'])
.config(configure)
.run(run);

configure.$inject = ['$stateProvider','$urlRouterProvider'];
run.$inject = ['$ionicPlatform'];

function configure($stateProvider, $urlRouterProvider){
  $stateProvider
    .state('lists',{
      url:'/lists',
      templateUrl:'view/lists.html',
      controller:'ListsCtrl',
      controllerAs:'vm'
    })

  $urlRouterProvider.otherwise('/lists')
};

function run($ionicPlatform) {
  $ionicPlatform.ready(function () {
    console.log("程序启动");
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
  })
};
