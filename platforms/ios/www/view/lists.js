/**
 * Created by ufogxl on 16/6/17.
 */

angular.module('Lists',[])
  .controller('ListsCtrl',ListCtrl);

ListCtrl.$inject = ["$scope","$ionicPopup"];

function ListCtrl($scope,$ionicPopup){

    $scope.delete = function(item){
      $scope.items.splice($scope.items.indexOf(item),1);
    };

    $scope.add = function () {
      console.log("32131" + $scope.items.slice(0,1).shift().id);
      if($scope.items.slice(0,1).shift().id>0) {
        $scope.items.unshift({id: $scope.items.slice(0,1).shift().id - 1});
      }else{
        var _id = $scope.items.pop().id;
        $scope.items.push({id:_id});
        $scope.items.push({id:_id+1})
      }
    };

    $scope.edit = function(item) {
      console.log("编辑");
      $scope.data = {}

      var myPopup = $ionicPopup.show({
        template: '<textarea rows="1" ng-model="data.text">',
        title: '更改id',
        scope: $scope,
        buttons: [
          {text: '取消'},
          {
            text: '<b>提交</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!$scope.data.text) {
                //don't allow the user to close unless he enters wifi password
                e.preventDefault();
              } else {
                item.id = $scope.data.text;
              }
            }
          }
        ]
      });
    };

      $scope.items = [
        { id: 0 },
        { id: 1 },
        { id: 2 },
        { id: 3 },
        { id: 4 },
        { id: 5 },
        { id: 6 },
        { id: 7 },
        { id: 8 },
        { id: 9 },
        { id: 10 },
        { id: 11 },
        { id: 12 },
        { id: 13 },
        { id: 14 },
        { id: 15 },
        { id: 16 },
        { id: 17 },
        { id: 18 },
        { id: 19 },
        { id: 20 },
        { id: 21 },
        { id: 22 },
        { id: 23 },
        { id: 24 },
        { id: 25 },
        { id: 26 },
        { id: 27 },
        { id: 28 },
        { id: 29 },
        { id: 30 },
        { id: 31 },
        { id: 32 },
        { id: 33 },
        { id: 34 },
        { id: 35 },
        { id: 36 },
        { id: 37 },
        { id: 38 },
        { id: 39 },
        { id: 40 },
        { id: 41 },
        { id: 42 },
        { id: 43 },
        { id: 44 },
        { id: 45 },
        { id: 46 },
        { id: 47 },
        { id: 48 },
        { id: 49 },
        { id: 50 }
      ];
    };
